package dao;

import domain.ContactEntity;
import domain.PersonEntity;
import factory.ConnectionFactory;

import java.sql.*;

public class XmlDaoImpl implements XmlDao {
    @Override
    public void save(PersonEntity person) {

        final String SQL = "insert into CUSTOMERS values (DEFAULT,?,?,?)";
        PreparedStatement statement;
        try {
            Connection conn = ConnectionFactory.connect();
            if (conn != null) {
                statement = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
                statement.setString(1, person.getName());
                statement.setString(2, person.getSurname());
                if (person.getAge() != null) {
                    statement.setLong(3, person.getAge());
                } else {
                    statement.setNull(3, Types.NUMERIC);
                }
                statement.executeUpdate();
                ResultSet rs = statement.getGeneratedKeys();
                if (rs.next()) {
                    person.setId(rs.getLong(1));
                }
                statement.close();
                conn.close();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void save(ContactEntity contact) {

        final String SQL = "insert into CONTACTS values (DEFAULT,?,?,?)";
        PreparedStatement statement;
        try {
            Connection conn = ConnectionFactory.connect();
            if (conn != null) {
                statement = conn.prepareStatement(SQL);
                statement.setObject(1, contact.getPerson().getId(), Types.INTEGER);
                statement.setInt(2, contact.getType());
                statement.setString(3, contact.getContact());

                statement.executeUpdate();

                statement.close();
                conn.close();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }


    }
}
