package dao;

import domain.ContactEntity;
import domain.PersonEntity;

public interface XmlDao {
    void save(PersonEntity person);
    void save(ContactEntity contact);

}
