package domain;

import lombok.Data;

import javax.xml.bind.annotation.*;
import java.util.List;
@Data
@XmlRootElement(name = "persons")
@XmlAccessorType (XmlAccessType.FIELD)
public class Persons {

    @XmlElement(name = "person")
    List<PersonEntity> persons;
}
