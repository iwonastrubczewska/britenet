package domain;

public enum Type {
   UNKNOW(0), EMAIL(1), PHONE(2), JABBER(3);

   private final int id;

   Type(int id) {
      this.id = id;
   }

   public int getValue(){
      return id;
   }
}
