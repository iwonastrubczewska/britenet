package domain;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.LinkedList;
import java.util.List;
@Data
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Contacts {

    private List<String> phone = new LinkedList<>();
    private List<String> email = new LinkedList<>();
    @XmlAnyElement(lax = true)
    private List<Object> unknow = new LinkedList<>();
    private List<String> jabber = new LinkedList<>();

}
