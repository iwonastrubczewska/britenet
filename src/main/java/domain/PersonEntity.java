package domain;

import lombok.Data;

import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@Table(name = "CUSTOMERS")
@XmlRootElement(name = "person")
@XmlAccessorType(XmlAccessType.FIELD)
public class PersonEntity extends AbstractEntity{

    private String name;
    private String surname;
    private Long age;
    private String city;
    List<Contacts> contacts;

}
