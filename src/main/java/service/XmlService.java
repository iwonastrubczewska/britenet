package service;

import domain.Contacts;
import domain.PersonEntity;
import domain.Persons;

public interface XmlService {

    void prepareData(Persons persons);

    void manageContactType(Contacts contact, PersonEntity person);
}
