package service;

import dao.XmlDao;
import dao.XmlDaoImpl;
import domain.*;

public class XmlServiceImpl implements XmlService {

    private XmlDao xmlDao;

    public XmlServiceImpl() {
        this.xmlDao = new XmlDaoImpl();
    }

    public void prepareData(Persons persons) {
        persons.getPersons().forEach(person -> {
            person.getContacts().forEach(contact -> {
                xmlDao.save(person);
                manageContactType(contact, person);
            });
        });
    }

    public void manageContactType(Contacts contact, PersonEntity person) {
        if (!contact.getEmail().isEmpty()) {
            contact.getEmail().forEach(email ->
                    xmlDao.save(new ContactEntity(person, Type.EMAIL.getValue(), email)));
        }
        if (!contact.getPhone().isEmpty()) {
            contact.getPhone().forEach(phone ->
                    xmlDao.save(new ContactEntity(person, Type.PHONE.getValue(), phone)));
        }
        if (!contact.getJabber().isEmpty()) {
            contact.getJabber().forEach(jabber ->
                    xmlDao.save(new ContactEntity(person, Type.JABBER.getValue(), jabber)));
        }
        if (!contact.getUnknow().isEmpty()) {
            contact.getUnknow().forEach(icq -> {
                org.w3c.dom.Element elem = (org.w3c.dom.Element) icq;
                elem.getFirstChild().getTextContent();
                xmlDao.save(new ContactEntity(person, Type.UNKNOW.getValue(), elem.getFirstChild().getTextContent()));
            });
        }
    }

}
