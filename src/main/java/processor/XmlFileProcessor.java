package processor;

import domain.Persons;
import org.xml.sax.SAXException;
import service.XmlService;
import service.XmlServiceImpl;

import javax.xml.bind.JAXB;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.io.InputStream;

public class XmlFileProcessor {

    private XmlService xmlService;

    public XmlFileProcessor() {
        this.xmlService = new XmlServiceImpl();
    }

    public void readAndParseXmlFile() throws XMLStreamException, IOException, JAXBException, ParserConfigurationException, SAXException {
        try {
            InputStream fis = getClass()
                    .getClassLoader().getResourceAsStream("file/dane-osoby.xml");
            Persons persons = JAXB.unmarshal(fis, Persons.class);
            xmlService.prepareData(persons);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}




