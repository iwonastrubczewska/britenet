package main.java;

import org.xml.sax.SAXException;
import processor.XmlFileProcessor;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        XmlFileProcessor xmlFileProcessor = new XmlFileProcessor();
        try {
            xmlFileProcessor.readAndParseXmlFile();
        } catch (XMLStreamException | JAXBException | SAXException | ParserConfigurationException | IOException e) {
            e.printStackTrace();
        }
    }
}
